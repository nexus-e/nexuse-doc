This documentation is shown on <https://nexus-e.readthedocs.io/en/latest/>.  

### How to compile the whole documentation locally
- (Only need to do it once) Create a python virtual environment for better python package management
	- Option 1: use [conda](https://conda.io/projects/conda/en/latest/index.html)
		- install conda following [this instruction](https://conda.io/projects/conda/en/latest/user-guide/install/index.html)
	    - `conda create -n nexuse-doc python=3.7` (you can change the "nexuse-doc" to any name for the virtual environment; the python version should be aligned with the version specified in [docs/.readthedocs.yml](https://gitlab.ethz.ch/nexus-e/nexuse-doc/-/blob/master/docs/.readthedocs.yml)) 
	    - `conda activate nexuse-doc`
		- `cd docs`
		- `pip install -r requirements.txt`
	- Option 2: use [virtualenv](https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/)
		- `pip install virtualenv`
		- `cd` to the repository
		- `virtualenv -p python3.7 venv` (The python version should be aligned with the version specified in [docs/.readthedocs.yml](https://gitlab.ethz.ch/nexus-e/nexuse-doc/-/blob/master/docs/.readthedocs.yml).) 
		- (On Mac or Linux) `source venv/bin/activate` or (on Windows) `venv\Scripts\activate.bat`
		- `cd docs`
		- `pip install -r requirements.txt`
- (Need to be done every time) Once you have created a virtual environment 
	- Activate the virtual environment, with **one of** the following commands
		- (for conda environment) `conda activate nexuse-doc`
		- (for virtualenv on Mac or Linux) `source venv/bin/activate` 
		- (for virtualenv on Windows) `venv\Scripts\activate.bat`
	- `cd docs`
	- `make html`
	- Open the compiled documentation in a browser
		- (for Mac or Linus) `open build/html/index.html`
		- Or, (for all operating systems) manually open the file build/html/index.html.

### Additional information
- How to deactivate a virtual environment  

	- Either: `conda deactivate` (for conda environment) 
	- Or: `deactivate` (for virtualenv).

- How to delete a virtual environment  

	- Either: `conda env remove --name nexuse-doc`(for conda environment)
	- Or: manually delete the `venv` folder in the repository (for virtualenv).

- How to update a python package  

	The compilation of the documentation requires some python packages as specified in `docs/requirements.txt`. To update a package, 
	- change its version number in `requirements.txt` 
	- `cd` to the repository
	- activate the virtual environment same as [above](#how-to-compile-the-whole-documentation-locally)
	- `cd` to the "docs" folder of this repository
	- `pip install -r requirements.txt`

	:warning: However, due to interdependencies among the packages, sometimes it is not possible to update individual packages (i.e., when the version number of one package is changed, but the new version is not anymore compatible with other packages). If this happens, 
	- create a new virtual environment
	- in the new virtual environment all required packages **without specifying any version number**. (By doing so the updateness and the compatibility of **all** packages will be guaranteed.) 
	- test the compiliation locally with the new virtual environment
	- if it works, update the `docs/requirements.txt` based on the versions of the packages in the new virtual environment. (With `pip freeze` you can see the version numbers of all the dependencies.)

- How to check whether a package needs to be (or can be) updated  

	Use `pip list --outdated`.

- How to manage dependencies with PyCharm

	(On Mac) Pycharm > Preferences > Project: nexuse-doc > Project Interpreter. Here you can specify or create a virtual environment (i.e., "Project Interpreter") and view the versions (plus the available updates) of each python package. Details on [how to configure a Python interpreter](https://www.jetbrains.com/help/pycharm/configuring-python-interpreter.html#add-existing-interpreter) and [how to user requirements.txt](https://www.jetbrains.com/help/pycharm/managing-dependencies.html) in Pycharm.
