### **Common issues**

#### Database
##### Grant full privileges to a user in MySQL Workbench
![](images/mysql-user-privileges.png)
![](images/mysql-user-privileges-2.png)

##### Access denied to access database
- In Matlab, print out the variables `connInfo.user`, `connInfo.pw`, and `connInfo.host` to make sure that they are exactly what you provided in the your `UserDBInfo` file.
- Make sure you database user account has [full permission to the database](#grant-full-privileges-to-a-user-in-mysql-workbench)

##### Database server timezone issue
There are two ways to solve this issue: (1) using command line, but this only takes effect for the duration of the current service uptime, i.e., when the MySQL services restarts, you need to type in these commands again; (2) editing the MySQL configuration file, but the configuration file is platform-dependent, e.g., MacOS doesn't even have a default configuration file.  

Here we introduce the 1st approach using command line. For the 2nd approace of editing the MySQL configuration file, see [here](https://phoenixnap.com/kb/change-mysql-time-zone).

- Open command prompt and login to mysql using the command:  
`mysql -u root -p`
- Enter your MySQl password for the root user, this should start mysql and you should see the prompt `mysql>`
- Type the following two command to reset the MySQL time zone:  
`SET @@global.time_zone = 'Europe/Paris';`  
`SET @@session.time_zone = 'Europe/Paris';`  
    - If you get the error "Unknown or incorrect time zone: 'XXX'", use the command ([ref](https://dba.stackexchange.com/questions/120945/how-do-i-resolve-this-error-error-1298-hy000-unknown-or-incorrect-time-zone)):  
    `mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root mysql –p`  
    Then rerun the `SET @@...` commands.
- You can check that these were properly set using the command:  
`SELECT @@global.time_zone, @@session.time_zone;`
- After confirming these are now correct you can exit mysql using the command:  
`exit`

##### Database definer missing
Add a dummy user (same name as the missing definer - you can see it from the error message) to the database and [grant all previleges](#grant-full-privileges-to-a-user-in-mysql-workbench) .

#### Software compatibility
##### Matlab & python
On Euler, sometimes you get a similar error `ImportError: ...pyexpat...: undefined symbol: XML_SetHashSalt`. This might be because of conflice between Matlab's and Python's  `libexpat.so.1` library. If you encounter this, it is better to consult IT. Or, you could try different combinations of Matlab and python by yourself.  
Here is an example to compare the versions of the `libexpat.so.1` library of Python 3.7.1 and Matlab R2020a:  
- Input command `ldd /cluster/apps/python/3.7.1/x86_64/lib64/python3.7/lib-dynload/pyexpat.cpython-37m-x86_64-linux-gnu.so`. It gives `libexpat.so.1 => /lib64/libexpat.so.1 (0x00002b00348bd000)`. This means the Python 3.7.1's library `pyexpat.cpython-37m-x86_64-linux-gnu.so` is linked against the operating system's `libexpat.so.1` library and not the one from Matlab (`/cluster/apps/matlab/R2020a/bin/glnxa64/libexpat.so.1`).
- The screenshot below shows that the operating system's library links to the version 1.6.0; while Matlab R2020a's library links to the version 1.6.9. Experiences show that when if Matlab has a newer version than Python (namely, newer than the operating system), it should work.  
![](images/euler-ldd-command.png)



