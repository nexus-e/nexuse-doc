# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

# Use a mobile-friendly theme: https://pypi.org/project/sphinx-rtd-theme/
import sphinx_rtd_theme

# To enable AutoStructify: https://github.com/readthedocs/recommonmark#autostructify
import recommonmark
from recommonmark.transform import AutoStructify


# -- Project information -----------------------------------------------------

project = 'Nexus-e'
copyright = '2021, Energy Science Center, ETH Zurich'
author = 'Nexus-e'

# The full version, including alpha/beta/rc tags
release = '0.0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
'recommonmark',
'sphinxemoji.sphinxemoji',
'sphinx_rtd_theme',
'sphinx_markdown_tables'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
html_theme_options = {
'display_version': True,
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
# custom stylesheet is _static/css/custom.css
html_css_files = [
'css/custom.css'
]
# html_logo = "images/logo.png"

# Based on https://github.com/readthedocs/readthedocs.org/issues/2569
master_doc = 'index'

# https://www.sphinx-doc.org/en/master/usage/markdown.html
source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}

# To enable AutoStructify: https://github.com/readthedocs/recommonmark#autostructify
#github_doc_root = 'https://github.com/Xuqian-Yan/nexuse-doc/docs/'
def setup(app):
    app.add_config_value('recommonmark_config', {
#            'url_resolver': lambda url: github_doc_root + url,
            'enable_eval_rst': True,
            }, True)
    app.add_transform(AutoStructify)

