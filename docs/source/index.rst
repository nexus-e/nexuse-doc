.. image:: images/logo.png
    :width: 200px
    
An integrated energy systems modelling platform | `nexus-e.org <http://nexus-e.org/>`_
========================================================================================

.. toctree::
   :maxdepth: 4

   setup.md
   euler_instruction.md
   nexuse_repo_instruction.md
   modify_input_data.md
   connect_new_module.md
   visualization_tool.md
   faq.md