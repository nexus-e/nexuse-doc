### **Connect new module**

- Before connecting a new module to the Nexus-e platform, the answer of following questions is needed:

	1.	Is it going to be a substitute/replacement for an existing module?
	2.	Is it an additional module that will be part of the existing or new convergence loops?

- In case question one is true, the existing interfaces can be used, including the convergence criteria that already exists. Furthermore, these interfaces may be extended if new points of connections are identified. This process requires consultation with the involved modelers.

- In case question two is true, clear interfaces has to be defined, i.e., the data exchange between the new module and the existing modules. Also, if the new module is part of any of the existing loops, the convergence criterion may need to be revised. Otherwise, new loop and convergence criteria need to be defined. These processes require consultation with the involved modelers.

- Each model within Nexus-e is connected to the platform using a wrapper, i.e., Matlab class. As an example use the existing wrappers, i.e., emModule.m (\eMark\electricity_markets\marketsModule) or CascadesModule.m (\Cascades\03_Run_models&methods). The class contains of properties and methods.

- In properties, assign your property attributes, e.g., wkspace (workspace information).

- In methods, we suggest that you set three types of functions:

	- Object constructor, which creates the module object. There you can have, e.g., additional paths to your subfolders, reed your input parameters, assign data structures (e.g., input parameters) to the module object, etc.
	- Run method, which executes the module. This function, besides the module object, as an inputs can use data from other modules with which interfaces are established.  
	- Interface functions, which gather the data which will be stored and later send to other modules. You can have multiple interface functions. The number of these functions depends on the number of modules and type of information your module is sending data to. It is not necessary that the data exchange between two modules is going in two directions.

- Note that the aforementioned instructions are general directions, and therefore, the class is subject to customization based on the needs and preferences of the modelers.
 
