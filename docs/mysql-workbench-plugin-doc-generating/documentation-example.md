# Schema documentation

Generated by [MySQL Workbench Model Documentation plugin](https://github.com/letrunghieu/mysql-workbench-plugin-doc-generating) - Copyright (c) 2015 Hieu Le

## Table: `DBinfo`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `Date` | VARCHAR(25) | Not null |   |   |
| `Excel_file_used` | VARCHAR(150) | Not null |   |   |
| `Matlab_file_used` | VARCHAR(150) | Not null |   |   |
| `created_by_user` | VARCHAR(100) | Not null |   |   |
| `Schema_version` | DOUBLE | Not null |   |   |
| `notes` | TEXT |  | `NULL` |   |




## Table: `busconfiguration`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idNetworkConfig` | INT | PRIMARY, Not null |   |  **foreign key** to column `idNetworkConfig` on table `networkconfiginfo`. |
| `idBus` | INT | PRIMARY, Not null |   |  **foreign key** to column `idBus` on table `busdata`. |
| `BusName` | VARCHAR(45) |  | `NULL` |   |
| `Vmax` | DOUBLE |  | `'1.1'` |   |
| `Vmin` | DOUBLE |  | `'0.9'` |   |
| `WindShare` | DOUBLE |  | `NULL` |   |
| `SolarShare` | DOUBLE |  | `NULL` |   |
| `idDistProfile` | INT |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idNetworkConfig`, `idBus` | PRIMARY |   |
| fk_BusDataConfiguration_BusData1_idx | `idBus` | INDEX |   |
| fk_BusDataConfiguration_NetworkConfigInfo1_idx | `idNetworkConfig` | INDEX |   |


## Table: `busdata`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idBus` | INT | PRIMARY, Not null |   |   |
| `internalBusId` | INT |  | `NULL` |   |
| `BusName` | VARCHAR(45) |  | `NULL` |   |
| `SwissgridNodeCode` | VARCHAR(45) |  | `NULL` |   |
| `ZoneId` | INT |  | `NULL` |   |
| `X_Coord` | FLOAT |  | `NULL` | X coordinate in LV03 format |
| `Y_Coord` | FLOAT |  | `NULL` | Y coordinate in LV03 format |
| `BusType` | VARCHAR(2) |  | `'PQ'` |   |
| `Qd` | DOUBLE |  | `'0'` |   |
| `Pd` | DOUBLE |  | `'0'` |   |
| `Gs` | DOUBLE |  | `'0'` |   |
| `Bs` | DOUBLE |  | `'0'` |   |
| `baseKV` | DOUBLE |  | `NULL` |   |
| `Country` | VARCHAR(45) |  | `NULL` |   |
| `SubRegion` | VARCHAR(45) |  | `NULL` |   |
| `StartYr` | DOUBLE |  | `NULL` |   |
| `EndYr` | DOUBLE |  | `NULL` |   |
| `WindShare` | DOUBLE |  | `NULL` |   |
| `SolarShare` | DOUBLE |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idBus` | PRIMARY |   |


## Table: `distprofiles`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idDistProfile` | INT | PRIMARY, Auto increments, Not null |   |   |
| `name` | VARCHAR(45) |  | `NULL` |   |
| `type` | VARCHAR(45) |  | `NULL` |   |
| `resolution` | VARCHAR(45) |  | `NULL` |   |
| `unit` | VARCHAR(45) |  | `NULL` |   |
| `timeSeries` | JSON |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idDistProfile` | PRIMARY |   |


## Table: `genconfiginfo`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idGenConfig` | INT | PRIMARY, Not null |   |   |
| `name` | VARCHAR(45) |  | `NULL` |   |
| `year` | INT |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idGenConfig` | PRIMARY |   |


## Table: `genconfiguration`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idGenConfig` | INT | PRIMARY, Not null |   |  **foreign key** to column `idGenConfig` on table `genconfiginfo`. |
| `idBus` | INT | PRIMARY, Not null |   |  **foreign key** to column `idBus` on table `busdata`. |
| `idGen` | INT | PRIMARY, Not null |   |  **foreign key** to column `idGen` on table `gendata`. |
| `GenName` | VARCHAR(55) |  | `NULL` |   |
| `idProfile` | INT |  | `NULL` |  **foreign key** to column `idProfile` on table `profiledata`. |
| `CandidateUnit` | TINYINT |  | `NULL` |   |
| `Pmax` | DOUBLE |  | `NULL` |   |
| `Pmin` | DOUBLE |  | `NULL` |   |
| `Qmax` | DOUBLE |  | `NULL` |   |
| `Qmin` | DOUBLE |  | `NULL` |   |
| `Emax` | DOUBLE |  | `NULL` |   |
| `Emin` | DOUBLE |  | `NULL` |   |
| `E_ini` | DOUBLE |  | `NULL` |   |
| `VOM_Cost` | DOUBLE |  | `NULL` | nonFuel variable O&M cost in Euro/MWh |
| `FOM_Cost` | DOUBLE |  | `NULL` | Fixed O&M cost in Euro/MW/year |
| `InvCost` | DOUBLE |  | `NULL` | annualised investment cost for generator in Euro/MW |
| `InvCost_E` | DOUBLE |  | `NULL` | annualized investment cost for storage capacity in Euro/MW |
| `StartCost` | DOUBLE |  | `NULL` |   |
| `TotVarCost` | DOUBLE |  | `NULL` |   |
| `FuelType` | VARCHAR(45) |  | `NULL` |   |
| `status` | DOUBLE |  | `NULL` |   |
| `HedgeRatio` | DOUBLE |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idGenConfig`, `idBus`, `idGen` | PRIMARY |   |
| fk_GenComposition_BusData1_idx | `idBus` | INDEX |   |
| fk_GenComposition_GenData1_idx | `idGen` | INDEX |   |
| fk_GenConfiguration_ProfileData1_idx | `idProfile` | INDEX |   |
| fk_GenConfiguration_GenConfigInfo1_idx | `idGenConfig` | INDEX |   |


## Table: `gendata`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idGen` | INT | PRIMARY, Not null |   |   |
| `GenName` | VARCHAR(55) |  | `NULL` | Unit name |
| `GenType` | VARCHAR(45) |  | `NULL` | Basic gen type (Hydro, Cons, RES) |
| `Technology` | VARCHAR(45) |  | `NULL` | Technology subtype (Dam, Pump, RoR, Nucl, Lignite, Coal, GasCC, GasSC, Biomass, Oil, Wind, PV, GeoTh, etc) |
| `UnitType` | VARCHAR(45) |  | `NULL` | Dispatchable or NonDispatchable (used to know which gens are controllable and can be used for reserves) |
| `StartYr` | DOUBLE |  | `NULL` | Year this generator was first online (default = 2012) |
| `EndYr` | DOUBLE |  | `NULL` | Last year this generator is online |
| `GenEffic` | DOUBLE |  | `NULL` | Fractional generator efficiency (MWh elec / MW heat) |
| `CO2Rate` | DOUBLE |  | `NULL` | CO2 emission rate (ton / MWh elec) |
| `eta_dis` | DOUBLE |  | `NULL` |   |
| `eta_ch` | DOUBLE |  | `NULL` |   |
| `RU` | DOUBLE |  | `NULL` | Ramp Up rate (MW/min) |
| `RD` | DOUBLE |  | `NULL` | Ramp Down Rate (MW/min) |
| `RU_start` | DOUBLE |  | `NULL` | Ramp Up Rate during Start Up (MW/min) |
| `RD_shutd` | DOUBLE |  | `NULL` | Ramp Down Rate during Shut Down (MW/min) |
| `UT` | INT |  | `NULL` | Minimum Up Time (hr) |
| `DT` | INT |  | `NULL` | Minimum Down Time (hr) |
| `Pini` | DOUBLE |  | `NULL` | Initial power generation level at first time interval of simulation (MW) |
| `Tini` | DOUBLE |  | `NULL` | Number of hours generator has already been online at first time interval of simulation (MW) |
| `meanErrorForecast24h` | DOUBLE |  | `NULL` | normalised mean error for renewable generation forecasted 24 hrs ahead (dimensionless) |
| `sigmaErrorForecast24h` | DOUBLE |  | `NULL` | standard deviation for renewable generation forecasted 24 hrs ahead (dimensionless) |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idGen` | PRIMARY |   |


## Table: `lineconfiguration`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idNetworkConfig` | INT | PRIMARY, Not null |   |  **foreign key** to column `idNetworkConfig` on table `networkconfiginfo`. |
| `idLine` | INT | PRIMARY, Not null |   |  **foreign key** to column `idLine` on table `linedata`. |
| `LineName` | VARCHAR(45) | Not null |   |   |
| `idFromBus` | INT | PRIMARY, Not null |   |  **foreign key** to column `idBus` on table `busdata`. |
| `idToBus` | INT | PRIMARY, Not null |   |  **foreign key** to column `idBus` on table `busdata`. |
| `angmin` | DOUBLE |  | `'-360'` |   |
| `angmax` | DOUBLE |  | `'360'` |   |
| `status` | DOUBLE |  | `NULL` |   |
| `FromBusName` | VARCHAR(45) |  | `NULL` |   |
| `ToBusName` | VARCHAR(45) |  | `NULL` |   |
| `FromCountry` | VARCHAR(45) |  | `NULL` |   |
| `ToCountry` | VARCHAR(45) |  | `NULL` |   |
| `Ind_CrossBord` | INT |  | `NULL` |   |
| `Ind_Agg` | INT |  | `NULL` |   |
| `Ind_HVDC` | INT |  | `NULL` |   |
| `Candidate` | TINYINT |  | `NULL` |   |
| `CandCost` | DOUBLE |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idNetworkConfig`, `idLine`, `idFromBus`, `idToBus` | PRIMARY |   |
| fk_LineConfiguration_BusData1_idx | `idFromBus` | INDEX |   |
| fk_LineConfiguration_BusData2_idx | `idToBus` | INDEX |   |
| fk_LineConfiguration_LineData1_idx | `idLine` | INDEX |   |
| fk_LineConfiguration_NetworkconfigInfo1_idx | `idNetworkConfig` | INDEX |   |


## Table: `linedata`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idLine` | INT | PRIMARY, Not null |   |   |
| `LineName` | VARCHAR(45) | Not null |   |   |
| `r` | DOUBLE |  | `NULL` |   |
| `x` | DOUBLE |  | `NULL` |   |
| `b` | DOUBLE |  | `NULL` |   |
| `rateA` | DOUBLE |  | `NULL` |   |
| `rateB` | DOUBLE |  | `NULL` |   |
| `rateC` | DOUBLE |  | `NULL` |   |
| `StartYr` | DOUBLE |  | `NULL` |   |
| `EndYr` | DOUBLE |  | `NULL` |   |
| `kV` | DOUBLE |  | `NULL` |   |
| `MVA_Winter` | DOUBLE |  | `NULL` |   |
| `MVA_Summer` | DOUBLE |  | `NULL` |   |
| `MVA_SprFall` | DOUBLE |  | `NULL` |   |
| `length` | DOUBLE |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idLine` | PRIMARY |   |


## Table: `loadconfiginfo`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idLoadConfig` | INT | PRIMARY, Not null |   |   |
| `name` | VARCHAR(45) |  | `NULL` |   |
| `year` | INT |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idLoadConfig` | PRIMARY |   |


## Table: `loadconfiguration`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idLoadConfig` | INT | PRIMARY, Not null |   |  **foreign key** to column `idLoadConfig` on table `loadconfiginfo`. |
| `idBus` | INT | PRIMARY, Not null |   |  **foreign key** to column `idBus` on table `busdata`. |
| `idLoad` | INT | PRIMARY, Not null |   |  **foreign key** to column `idLoad` on table `loaddata`. |
| `idProfile` | INT |  | `NULL` |  **foreign key** to column `idProfile` on table `profiledata`. |
| `loadFactor` | DOUBLE |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idLoadConfig`, `idBus`, `idLoad` | PRIMARY |   |
| fk_LoadConfiguration_LoadData1_idx | `idLoad` | INDEX |   |
| fk_LoadConfiguration_BusData1_idx | `idBus` | INDEX |   |
| fk_LoadConfiguration_ProfileData1_idx | `idProfile` | INDEX |   |
| fk_LoadConfiguration_LoadConfigInfo1_idx | `idLoadConfig` | INDEX |   |


## Table: `loaddata`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idLoad` | INT | PRIMARY, Not null |   |   |
| `LoadType` | VARCHAR(45) |  | `NULL` |   |
| `Pd` | DOUBLE |  | `NULL` |   |
| `Qd` | DOUBLE |  | `NULL` |   |
| `hedgeRatio` | DOUBLE |  | `NULL` |   |
| `meanForecastError24h` | DOUBLE |  | `NULL` |   |
| `sigmaForecastError24h` | DOUBLE |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idLoad` | PRIMARY |   |


## Table: `marketsconfiguration`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idMarketsConfig` | INT | PRIMARY, Not null |   |   |
| `name` | VARCHAR(45) |  | `NULL` |   |
| `year` | INT |  | `NULL` |   |
| `MarketsConfigDataStructure` | JSON |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idMarketsConfig` | PRIMARY |   |


## Table: `networkconfiginfo`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idNetworkConfig` | INT | PRIMARY, Not null |   |   |
| `name` | VARCHAR(45) |  | `NULL` |   |
| `year` | INT |  | `NULL` |   |
| `baseMVA` | DOUBLE |  | `NULL` |   |
| `MatpowerVersion` | VARCHAR(1) |  | `'2'` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idNetworkConfig` | PRIMARY |   |


## Table: `profiledata`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idProfile` | INT | PRIMARY, Auto increments, Not null |   |   |
| `name` | VARCHAR(45) |  | `NULL` |   |
| `year` | INT |  | `NULL` |   |
| `type` | VARCHAR(45) |  | `NULL` |   |
| `resolution` | VARCHAR(45) |  | `NULL` |   |
| `unit` | VARCHAR(45) |  | `'MW'` |   |
| `timeSeries` | JSON |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idProfile` | PRIMARY |   |


## Table: `projections`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `item` | VARCHAR(15) | Not null |   |   |
| `scenario` | VARCHAR(15) | Not null |   |   |
| `year` | DOUBLE | Not null |   |   |
| `value` | DOUBLE | Not null |   |   |




## Table: `renewabletargetconfiginfo`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idRenewTargetConfig` | INT | PRIMARY, Not null |   |   |
| `name` | VARCHAR(45) |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idRenewTargetConfig` | PRIMARY |   |


## Table: `renewabletargetdata`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idRenewTargetConfig` | INT | PRIMARY, Not null |   |   |
| `Year` | DOUBLE | PRIMARY, Not null |   |   |
| `renewTarget` | DOUBLE |  | `'0'` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idRenewTargetConfig`, `Year` | PRIMARY |   |


## Table: `scenarioconfiguration`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idScenario` | INT | PRIMARY, Not null |   |   |
| `idNetworkConfig` | INT | PRIMARY, Not null |   |  **foreign key** to column `idNetworkConfig` on table `networkconfiginfo`. |
| `idLoadConfig` | INT | PRIMARY, Not null |   |  **foreign key** to column `idLoadConfig` on table `loadconfiginfo`. |
| `idGenConfig` | INT | PRIMARY, Not null |   |  **foreign key** to column `idGenConfig` on table `genconfiginfo`. |
| `idMarketsConfig` | INT | PRIMARY, Not null |   |  **foreign key** to column `idMarketsConfig` on table `marketsconfiguration`. |
| `idRenewTargetConfig` | INT | PRIMARY, Not null |   |   |
| `name` | VARCHAR(100) |  | `NULL` |   |
| `runParamDataStructure` | JSON |  | `NULL` |   |
| `Year` | INT | PRIMARY, Not null |   |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idScenario`, `idNetworkConfig`, `idLoadConfig`, `idGenConfig`, `idMarketsConfig`, `idRenewTargetConfig`, `Year` | PRIMARY |   |
| fk_scenarioConfiguration_NetworkConfigInfo1_idx | `idNetworkConfig` | INDEX |   |
| fk_scenarioConfiguration_LoadConfigInfo1_idx | `idLoadConfig` | INDEX |   |
| fk_scenarioConfiguration_GenConfigInfo1_idx | `idGenConfig` | INDEX |   |
| fk_scenarioConfiguration_marketsConfiguration1 | `idMarketsConfig` | INDEX |   |


## Table: `transformerconfiguration`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idNetworkConfig` | INT | PRIMARY, Not null |   |  **foreign key** to column `idNetworkConfig` on table `networkconfiginfo`. |
| `idTransformer` | INT | PRIMARY, Not null |   |  **foreign key** to column `idTransformer` on table `transformerdata`. |
| `TrafoName` | VARCHAR(45) | Not null |   |   |
| `idFromBus` | INT | PRIMARY, Not null |   |  **foreign key** to column `idBus` on table `busdata`. |
| `idToBus` | INT | PRIMARY, Not null |   |  **foreign key** to column `idBus` on table `busdata`. |
| `angmin` | DOUBLE |  | `'-360'` |   |
| `angmax` | DOUBLE |  | `'360'` |   |
| `status` | DOUBLE |  | `NULL` |   |
| `FromBusName` | VARCHAR(45) |  | `NULL` |   |
| `ToBusName` | VARCHAR(45) |  | `NULL` |   |
| `FromCountry` | VARCHAR(45) |  | `NULL` |   |
| `ToCountry` | VARCHAR(45) |  | `NULL` |   |
| `Candidate` | TINYINT |  | `NULL` |   |
| `CandCost` | DOUBLE |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idNetworkConfig`, `idTransformer`, `idFromBus`, `idToBus` | PRIMARY |   |
| fk_TransformerConfiguration_BusData2_idx | `idFromBus` | INDEX |   |
| fk_TransformerConfiguration_TransformerData1_idx | `idTransformer` | INDEX |   |
| fk_TransformerConfiguration_NetworkconfigInfo1_idx | `idNetworkConfig` | INDEX |   |
| fk_TransformerConfiguration_BusData1_idx | `idToBus` | INDEX |   |


## Table: `transformerdata`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `idTransformer` | INT | PRIMARY, Not null |   |   |
| `TrafoName` | VARCHAR(45) | Not null |   |   |
| `r` | DOUBLE |  | `NULL` |   |
| `x` | DOUBLE |  | `NULL` |   |
| `b` | DOUBLE |  | `NULL` |   |
| `rateA` | DOUBLE |  | `'0'` |   |
| `rateB` | DOUBLE |  | `'0'` |   |
| `rateC` | DOUBLE |  | `'0'` |   |
| `tapRatio` | DOUBLE |  | `'1'` |   |
| `angle` | DOUBLE |  | `'0'` |   |
| `StartYr` | DOUBLE |  | `NULL` |   |
| `EndYr` | DOUBLE |  | `NULL` |   |
| `MVA_Winter` | DOUBLE |  | `NULL` |   |
| `MVA_Summer` | DOUBLE |  | `NULL` |   |
| `MVA_SprFall` | DOUBLE |  | `NULL` |   |
| `length` | DOUBLE |  | `NULL` |   |


### Indices: 

| Name | Columns | Type | Description |
| --- | --- | --- | --- |
| PRIMARY | `idTransformer` | PRIMARY |   |


## Table: `workforce`

### Description: 



### Columns: 

| Column | Data type | Attributes | Default | Description |
| --- | --- | --- | --- | ---  |
| `popscen` | VARCHAR(45) | Not null |   |   |
| `year` | DOUBLE | Not null |   |   |
| `value` | DOUBLE | Not null |   |   |





