# MySQL Workbench Model Document Generation

A Python script to generate documentation from MySQL Workbench ERR diagram. This README file is adpated based on the README.md from [this repository](https://github.com/letrunghieu/mysql-workbench-plugin-doc-generating/releases/tag/1.0.2) (retrieved on Apriil 16, 2021). The codes in mysql-workbench-plugin-doc-generating.py is slightly adjusted.

## Installation

* Open the MySQL Workbench
* Navigate to menu **Scripting** > **Install Plugin/Module...**
* Browse and select the extracted `mysql-workbench-plugin-doc-generating.py` file in this folder
* Restart the Workbench

NOTE: If you use Windows, it is possible that after the installation of the Plugin the Workbench will not start. This has been observed with versions 8.0.24 and 8.0.25. Version 8.0.18 works well.

## Usage

An example output of the Nexus-e database can be found at [documentation-example.md](documentation-example.md).

### Generate documentation from ERR digram

* Open the ERR digram
* Navigate to menu **Tools** > **Utilities** > **Generate Documentation (Markdown)**
* When you see the status bar text changed to *Documentation generated into the clipboard. Paste it to your editor.*, paste to (i.e., **overwrite**) the file `docs/sources/nexus-e-database.md`.

### Generate ERR digram from physical database

In case that you do not have the ERR diagram, you have to create a diagram from your physical database first. Don't worry, MySQL Workbench has a greate tool to do this for you called **Reverse Engineer**.

* Open Workbench
* Navigate to menu **Database** > **Reverse Engineer...**
* Choose the connection, **Next**
* Wait and **Next**
* Select the datbase you want to create ERR diagram from, **Next**
* Wait and **Next**
* Select tables that you want to include in the ERR diagram, **Execute>**
* Wait and **Next**
* **Finish**

You have a new ERR diagram, you can generate the documentation from this diagram as the previous step.


After that, you can convert the output Markdown document into any format that you want.


## License

This script is released under the MIT license.
